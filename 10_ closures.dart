///
/// Closures
///
main(List<String> args) {
  // A closure is a function that has access to the parent scope
  String message = "Dart is good";
  Function showMessage = () {
    message = "Dart is awesome";
    print(message);
  };
  showMessage();
  print(message);

  // A closure is a function object that has access to variables in its lexical scope
  Function talk = () {
    String msg = "Hi";

    Function say = () {
      msg = "Hello";
      print(msg);
    };

    return say;
  };

  Function speak = talk();
  speak();

  talk()();
}
