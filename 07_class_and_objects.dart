///
/// class and objects
///
main(List<String> args) {
  var student = Student(1);
  student.name = "Peter";
  student.study();

  var tom = Student(2, name: 'Tom');
  tom.study();

  var robot = Student.myCustomConstructor();
  robot.study();

}

class Student {
  // 下划线开头的表示私有(private)
  int _id = -1;
  String name;

  // 构造函数赋值
  Student(this._id, {this.name});

  // 自定义构造函数
  Student.myCustomConstructor() {
    _id = 0;
    name = 'Robot';
  }

  // 属性(读)
  int get id => _id;

  void study() {
    print("${this.name}(No.$_id) is now studying");
  }
}
