///
/// functions
///
main(List<String> args) {
  var n = add(200, 100);
  print(n);

  var m = minus(200, 100);
  print(m);

  output('Hello');

  func(1, 2, 3);
  func(4, 5);
  func(6);

  print('');

  func2(10, a: 1, b: 2);
  func2(0, b: 3);

  print('');

  fun3(1);
  fun3(1, y: 2);
}

int add(int x, int y) {
  return x + y;
}

// 函数返回值类型为dynamic
minus(int x, int y) {
  return x - y;
}

// '=>' 等价于 '{return xxxx;}'
output(var s) => print(s);

// Optional Parameters
func(int a, [int b, int c]) {
  print("a is $a");
  print("b is $b");
  print("c is $c");
}

// Optional Named Parameters
func2(int x, {int a, int b}) {
  print("x is $x");
  print("a is $a");
  print("b is $b");
}

// Optional Default Parameters
fun3(int x, {int y = 10}) {
  print("x is $x");
  print("y is $y");
}
