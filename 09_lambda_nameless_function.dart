///
/// lambda nameless function
///
main(List<String> args) {
  // Defining Lambda
  Function addFunc = (int a, int b) {
    var sum = a + b;
    print(sum);
  };

  var addFunc2 = (int a, int b) => a + b;

  addFunc(2, 3);
  print(addFunc2(3, 4));

  var func = addFunc3;
  func(1, 2);

  var newFunc = makeAddFunc();
  newFunc(2, 1);

  printAddResult(addFunc2, 5, 5);
}

// 普通函数
addFunc3(int x, int y) {
  var sum = x + y;
  print(sum);
}

// 返回一个'Function'
Function makeAddFunc() {
  return (int x, int y) {
    var sum = x + y;
    print(sum);
  };
}

// 接收一个'Function'类型的参数
printAddResult(Function addFunc, int x, int y) {
  var sum = addFunc(x, y);
  print(sum);
}
