# Dart语言入门

## Dart语言介绍
dart语言是由谷歌公司开发的网络编程语言，于2011年10月10日发布。可以通过官网进一步[了解Dart语言](https://www.dartlang.org/guides/language/language-tour)

## Dart开发环境安装和配置
操作系统：Windows、macOS、Linux
下载地址：https://flutter.dev/docs/development/tools/sdk/releases?tab=macos

* 以macOS为例：解压缩到目录/Users/(macuser)/Dev/flutter/

* 配置：
> vi ~/.bash_profile

* 编辑：
```bash
# 导出dart
export DART_HOME=/Users/(macuser)/Dev/flutter/flutter/bin/cache/dart-sdk

# 导出flutter
export FLUTTER_HOME=/Users/(macuser)/Dev/flutter/flutter
export PUB_HOSTED_URL=https://pub.flutter-io.cn
export FLUTTER_STORAGE_BASE_URL=https://storage.flutter-io.cn

export PATH=$DART_HOME/bin:$FLUTTER_HOME/bin:(其他导出项)
```

* 校验：
> dart --version


## 配置编辑器：VS Code
* 安装dart支持
![安装dart](screenshots/dart.jpg)

* 安装代码运行插件
![](screenshots/code_runner.jpg)
![](screenshots/run.jpg)

* 配置debug
![](screenshots/debug.jpg)

* 安装flutter（可选）
![flutter](screenshots/flutter.jpg)

## 主要内容
1. Hello World
    - 运行Dart代码
    - Dart语言介绍
2. 内置数据类型
    - 内置数据类型
    - 变量定义
3. 条件控制语句
    - IF ELSE 
    - 条件表达式
    - Switch Case 
4. 循环
    - for
    - while
    - do ..while
    - break
    - continue
    - 高级用法
5. 函数
    - 函数定义
    - 可选参数
    - 命名参数
    - 默认参数
6. 异常处理
    - try ..on
    - try ..catch
    - try ..finally
    - 自定义异常
7. 类和对象 
    - 定义类
    - 类成员可见性
    - 属性
    - 构造函数
      - 自定义构造函数
      - 成员变量赋值
8. 继承
    - 继承
    - extends、implements
    - 混入（mixin）
9. Lambda表达式
    - Lambda Expression
    - 函数返回Function
    - 函数接收Function类型的参数
10. 闭包
11. 集合 
12. callable classes
13. 设计模式（13-40）
    - 常用设计模式的Dart实现
