///
/// Hello Dart World
///
void main() {
  var lang = "Dart";

  /**
   * 多行
   * 注释
   */
  print("Hello $lang World"); // 单行注释

  ///
  /// 多行
  /// 注释
  ///
  print("This is my first $lang application");

  print(3 / 24);
  print(true);
}
