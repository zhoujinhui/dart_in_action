///
/// callable classes
///
main(List<String> args) {
  var call = Callable();
  call('Dart');
}

class Callable {
  call(String name) => print('call $name');
}
