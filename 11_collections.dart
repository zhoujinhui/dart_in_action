///
/// Collections
///
main(List<String> args) {
  // Fixed-length list
  List<int> fixedNumList = List(5);
  fixedNumList[0] = 10;
  fixedNumList[1] = 11;
  fixedNumList[2] = 12;
  fixedNumList[3] = 13;
  fixedNumList[4] = 14;
  print(fixedNumList);

  fixedNumList[0] = 100;
  fixedNumList[1] = null;
  print(fixedNumList);

  for (var item in fixedNumList) {
    print(item);
  }

  fixedNumList.forEach((element) => print(element ?? 'null value'));

  print('length: ${fixedNumList.length}');

  print('');

  // Growable list
  List<String> growableList = ['aaa', 'bbb', 'ccc'];
  growableList.add('ddd');
  growableList.add('eee');
  growableList.addAll(['fff', 'ggg']);
  print(growableList);

  growableList.remove('ggg');
  growableList.removeAt(2);
  print(growableList);

  growableList.clear();
  print(growableList);

  growableList..add('111')..add('222')..add('333');
  print(growableList);

  // Map
  Map<String, int> fruits = {"apple": 1, "banana": 2, "guava": 3};
  fruits["pear"]=4;
  print(fruits);
  fruits.forEach((k, v) => print('key:$k,value:$v'));

  // Set
  Set<String> countries = Set.from(["USA", "INDIA", "CHINA"]); // from a list
  countries.add("Japan");
  print(countries);
}
